import React, { useState, useRef } from 'react';
import { v4 as uuidv4 } from 'uuid';
import './App.css';
import TodoList from './TodoList';


function App() {
  type Todo ={
    id:number,
    name:string,
    isCompleted:boolean
  }
  const [name, setName]= useState<string>("");
  const [todos,setTodos]= useState<Todo[]>([]);

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>)=>{
  e.preventDefault()
  const newTodo:Todo = {
    id:todos.length,
    name:name,
    isCompleted:false
  }
  setTodos([...todos,newTodo])
  setName("")
  }
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>)=>{
  setName(e.target.value)
  }
  const handleEdit = (id:number,name: string)=>{
    const newTodos = todos.map((todo)=> {
      if(todo.id === id){
        todo.name = name
      }
      return todo
    }
    )
    setTodos(newTodos)
  }
  const handleChecked = (id:number,checked: boolean)=>{
    const newTodos = todos.map((todo)=> {
      if(todo.id === id){
        todo.isCompleted = !checked;
      }
      return todo
    }
    )
    setTodos(newTodos)
  }
  const handleDelete = (id:number)=>{
    const newTodos = todos.filter((todo)=> todo.id != id)
    setTodos(newTodos);
  }
  return (
    <div className="App">
      <h2>Todo lisy with typescript</h2>
    <form onSubmit={(e)=>handleSubmit(e)}>
      <input type="text" onChange={(e)=>handleChange(e)} className="inputText"/>
      <input type="submit" className="submitButton"/>
    </form>
    <ul className="todoList">
    {todos.map((todo)=>(
      <li key={todo.id}>
        <input type="text"  onChange={(e)=>handleEdit(todo.id,e.target.value)} className="inputText" value={todo.name} disabled ={todo.isCompleted}/>
        <input type="checkbox"  onChange={()=>handleChecked(todo.id,todo.isCompleted)} />
        <button onClick={()=>{handleDelete(todo.id)}} >Delete</button>
      </li>
    ))}
    </ul>
    </div>
  );
}

export default App;
